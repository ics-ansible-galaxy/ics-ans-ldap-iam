import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ldapservers')


def test_slapd(host):
    service = host.service("slapd")
    assert service.is_running
    assert service.is_enabled
